---
layout: post
title: "Full name of the CTF Year - Name of the task"
mathjax: true

date: 2023-01-04
---

*This is the solution of the challenge "Custom GCM" given during the [Black Alps 2022 CTF](https://www.blackalps.ch/ba-22/). Write a small excerpt that will show on the first page of the blog.*

<!--more-->

### Description

*Description
of theover
task*

### Details

Points:      100

Category:    forensic

Validations: 50

### Solution

We were given a file called usb.pcap

``` python
#!/usr/bin/env python2

from scapy.all import *

pcap = rdpcap("usb.pcap")
for i,p in enumerate(pcap):
	if len(p) > 100:
		open(str(i),"wb").write(p.load[27:])
```

After analyzing those files, we found that there is **two** files in the transfer.

# create the files1

After running our script we were left with two files:
```bash
file files1.ods 
files1.ods: OpenDocument Spreadsheet
```
After opening the first file with [Libreoffice](https://fr.libreoffice.org/) we
were greeted by:

![A5/1](/resources/2016/ndh/catch_me_if_you_can/screen_file1.png "A5/1")

Fun isn't it...

Digging in the 2nd file is more profitable, it show us a sort of table with
alphabetic and letter:

![A5/1](/resources/2016/ndh/catch_me_if_you_can/screen_file1.png "A5/1")

Using this code with the weird alphbetical table give us the flag: **ndh[wh3re1sw@lly]**.


