import hashlib

from musig import EC, G, Hash_non, key_aggregation, Hash_sig, Hash_agg

# Musig
my_pubkey = EC(0x7d29a75d7745c317aee84f38d0bddbf7eb1c91b7dcf45eab28d6d31584e00dd0, 0x25bb44e5ab9501e784a6f31a93c30cd6ad5b323f669b0af0ca52b8c5aa6258b9)    
Bob_pubkey = EC(0x206aeb643e2fe72452ef6929049d09496d7252a87e9daf6bf2e58914b55f3a90, 0x46c220ee7cbe03b138a76dcb4db673c35e2ab81b4235486fe4dbd2ad093e8df4)
Charlie_pubkey = EC(0xab44fe53836d50fa4b5755aa0683b5a61726e508a1ca814a93e1eab7122abdea, 0x4cbd1496aa36fc016bfe7b12c9fb8bb78eacab6f3655c586604250bb870cdaf1,)
Dany_pubkey = EC(0xb1c1e7545483dce5567345a7cf12d1c0a6bcbd0637b81f4082453a9bd89bd701, 0xb01d4cadf75b8ce3e05eda73a81a7c5cfb67618950e60657d61d4a44d2115dc7)

# From log.txt
m = [0] * 5
m[0] = b'250 grammes de beurre'
m[1] = b'250 grammes de farine blanche'
m[2] = b'250 grammes de sucre en poudre'
m[3] = b'4 oeufs de poules frais'
m[4] = b'11 grammes de levure chimique et quelques grains de sel'

s = [0] * 5
s[0] = 0x57c314c11adfe86309032c70f227339866e8e47fed91e133e89556f218235d8
s[1] = 0x21a494d6d53a19960c26c6233396923d1b02b966fc2aac454a34de16962f8f4c
s[2] = 0xb353f7f1737c4674317055b86fb6fbd271a8d34e2dd2809fc97a4cc09bbfcb3f
s[3] = 0x4972416b4e73b103cf432b0ab04bf6e8b1f9dcdc86635ffa932226b231c03be5
s[4] = 0xdc0c4ae900a736ad2e5ce285eb1fb378bb15700d118a8fcf4ab075e000e54ba

Rs = [0] * 5
Rs[0] = [EC(0xca0216f379a499e2e9773245267e3d7b1245750de4358ac2499b66ae0f45c211,    0xbf9e67581992eb02a12b795cce5d6bdc794c1ee8129006a665dc958754773cce),
        EC(0xe638277ca481b3ca881c1fde1d6fdcf671466cc6e9d0de8c9f083607b4645362,0x957900e8140f57fcb9f4cad268ef84dc77fa34ea80e8274642ea07a1c3edb55f),
        EC(0x453e7e221a5361e722c229f5faaedbd9495ca8f5b63f201fa47eef9878ac04a2,0xecfceaadd2591bd759e1a751b3740be6d21601ff8e925332b8963393f868f453),
        EC(0xf880396f3eb021d6d4b71fc883f8855c6e6288c3bf148b888ed0fbba33c3531f,0x5a5d3c45571217f5fcdc5d7feccb5dba626c2581bf962cc41f9f520435d8964a)]
Rs[1] = [EC(0x66c161685e672770546765482b854464189859bcf12892eb5f3ddf76c83811c,0xc7e9cac84db68fcb74ff89687101008489ba0a8f57c18e2a708a4c5554d255b8),
         EC(0xfa71f4fa3f9ad2b106287c77b46bfb358120e074a57dceba623b40ab2d9f4ce4,0xdce7d49abce1272507dff28695973455cf41e5c0b41094d77b2c1dd1d7df4479),
         EC(0xc895732ad7320d47ab9a710b163c56e6c99bb53c93f1a5043dd584424c77f36e,0x2a71c47194350c5814a964bae705d71bf658aadc601eed1a0ab57af797d156b1),
         EC(0xfa5b467080722ac45feaf0134a97f5d784905f6dd857152ad28a8f59edd37060,0x9c379aed2effa2ed8e5c27a710b33e925dd482c251a364afa5561a56a7bb3838)]
Rs[2] = [EC(0x6efbcbb330c502d0d308a7555804c506e7ac54db8edb86a18b5061a1bfafc083,0xeab2271effcdecc216865db505ddd431ade63fee6e110b380da0a0775a09b999),
         EC(0x5ea470bc53ca75ab0952d0c90404328898ce74735a269807a2cf81d5be9598c1,0x23477afd9ddf8f3b21c6aeb37fbf055d3ac330eefb622cd50400fcb0f6affc32),
         EC(0xb592139684a86c5d506c3b12238ad3cc0c8178c4d0f211461b341f2fd95e10ae,0x7157750ff755b549b61e5d7bf064e37cabee538e6dd0fff253e99f1f1e9a35b2),
         EC(0x99aa5bbedfd05e6b9954c3e05e90920991340a2584b03c7cbcff33399c963390,0xea15128255cd1264e7167cd95c2c8e22132c8dab6bf9bd08fc0bb16ef6abfe6f)]
Rs[3] = [EC(0x1e13d65348826e8560d33b81efeecc6464e2e852983c9c54b8188e792552a1d4,0x5ae4b1cb8194f89bf0c8f7ae0f2aab4822c5ba486a566ae3fdfa67a2ef702df8),
         EC(0x8f60a83b18d4f5499b66220287079b15a970bfd6d17734192509434e6ef91b8,0x3a39131b3b4c448b1a489784fd93bfa73017fcb75b96d5dcb8e8e8de76717a3c),
         EC(0xcc0e380f6bb855f7af59681ac47060f3b5005e70ef9b8a520f0430162ec95cc7,0xaa2b78ba7d9d846e3dc14b45caff038c4bb023e63575c8357ca3993e9b9fe6c2),
         EC(0xd47b6cfc7c1086a1faa0e04f10d5823ba87bb8ed437893fa6914e60df7809f2d,0xfcff3742e7fdc5209a42b28d4f66777baf24f2704d5c69b6dfc48bb3faa974d1)]
Rs[4] = [EC(0xd32900b4d5d56642673c40bff1b6de499ec45317afd20627395f8f50d6946b5d,0x9fe12195a34dd1e48ba3b612262e850d521ebd926e1331645c56571e9c903e4b),
         EC(0x1356f601d92abaf3c9d1bdf5bf265140cc48658391ab3c27250d96b4406c3338,0x9f32e9cd35b9adc222d4c68b16f85bdc988e61001942fd1fbcc026ad0344ae26),
         EC(0xe55cd82515e43d2d90a11254913fb4fe03ad9d06d933f2c42b70d845279b2550,0xc0ad7c801aa4ff614be15ac831c4e55d2ae4931a64c026344c501c23707a9b00),
         EC(0xed74bc9246ad25f2b2a8b0fa237a2ab39b97dd314ac1a532b17ca563a1b64fe8,0x3a64fb5072f7b7cdd684a35869dd91acc8b6cf0cb47d97e9d349eac4620f70e3)]
L = [my_pubkey, Bob_pubkey, Charlie_pubkey, Dany_pubkey]
a = Hash_agg(L,my_pubkey)

X = key_aggregation(L)
M = Matrix(GF(EC.order()), 5, 5)
v = vector(GF(EC.order()), s)

digest = []
for i in range(1,5):
    digest.append(int.from_bytes(hashlib.sha256(i.to_bytes(32,byteorder="big")).digest(),byteorder="big"))

for j in range(5):
    row = [0] * 5
    b = Hash_non(X, Rs[j], m[j])

    R = 0*G
    for i in range(len(L)):
        exp = pow(b, i, EC.order())
        R += exp* Rs[j][i]

    c = Hash_sig(X, R, m[j])

    row[0] = (c*a) % EC.order()
    m_int = int.from_bytes(m[j], "big")

    for i in range(4):
        row[i+1] = (pow(m_int, digest[i], EC.order()) * pow(b,i,EC.order())) % EC.order()
   
    M[j] = row

x = M.solve_right(v)
print(int(x[0]).to_bytes(32,"big"))
print(hex(x[0]))
print(x[0]*G == my_pubkey)