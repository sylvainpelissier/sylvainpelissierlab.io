import hashlib
# Hashes the message with SHA256 and casts the result into an integer.
def h(m):
    return int(hashlib.sha256(m).hexdigest(),16)

# Signs the message <m> using the point <G>
# <G> has to be a point on an elliptic curve of order <n>
# <x> is the Schnorr private key
# Returns the deterministic Schnorr signature of <m>
def sign(G, m, n, x):
    F = Integers(n)
    k = h(str(x).encode() + str(m).encode())
    (x1,y1) = (k*G).xy()
    r = F(x1)
    e = h(str(r).encode() + str(m).encode())
    return (F(k - x*e), F(e))

#p384 params
def params():
    p = int("0xfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeffffffff0000000000000000ffffffff", 16)
    n = int("0xffffffffffffffffffffffffffffffffffffffffffffffffc7634d81f4372ddf581a0db248b0a77aecec196accc52973", 16)
    a = int("0xfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeffffffff0000000000000000fffffffc", 16)
    b = int("0xb3312fa7e23ee7e4988e056be3f82d19181d9c6efe8141120314088f5013875ac656398d8a2ed19d2a85c8edd3ec2aef", 16)
    E = EllipticCurve(GF(p), [a,b])
    Gx, Gy = int("0xaa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7", 16), int("0x3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f",16)
    G = E(Gx,Gy)
    return (p, E, G, n)

#ECSchnorr Key Generation
# <G> has to be a point of an elliptic curve of order <n>
# Return the private key a and the public key A 
def keyGen(G, n):
    x = Integers(n).random_element().lift()
    A = G*x
    return (x, A)

#Computes the flag given a signature s,e
def getFlag(s,e):
    return "BA23{" + hashlib.md5(str(s).encode()  + str(e).encode()).hexdigest() +"}"
