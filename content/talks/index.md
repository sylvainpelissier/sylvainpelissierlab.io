---
title: "Publications"
menu:
  main:
    weight: 1
---

# Talks
* Sylvain Pelissier and Karim Sudki [*Uncovering more crypto secrets*](https://github.com/radareorg/r2con2024/blob/main/day2-talks/1015-r2con2024-UncoverMoreCrypto-sylvain-azox.pdf). **r2con 2024**
* Tommaso Gagliardoni, Marco Macchetti and Sylvain Pelissier. [*A Hitchhiker's Guide to Cryptography Code Audit*](https://csrc.nist.gov/presentations/2024/crclub-2024-01-24). **NIST Crypto Reading Club** talk on 2024 January 2024.
* Sylvain Pelissier, [*Tales of a quest for hash function vulnerabilities*](https://www.barbhack.fr/2023/assets/slides/tales-of-a-quest-for-hash-function-vulnerabilities.pdf). **Barbhack 2023**
* Sylvain Pelissier and Yolan Romailler, [*Practical exploitation of cryptographic flaws in Windows*](https://github.com/kudelskisecurity/northsec_crypto_api_attacks/blob/main/presentation/Practical%20exploitation%20of%20cryptographic%20flaws%20in%20Windows.pdf). **NorthSec 2023**
* Nicolas Oberli and Sylvain Pelissier, [*Hardware attacks against SM4 in practice*](https://hardwear.io/netherlands-2022/presentation/hardware-attacks-against-SM4-in-practice.pdf). **Black Alps 2022** and **Hardwear.io NL 2022**
* Sylvain Pelissier and Nils Amiet, [*Analyse forensique de la mémoire de GnuPG*](https://www.sstic.org/media/SSTIC2022/SSTIC-actes/gnupg_memory_forensics/SSTIC2022-Slides-gnupg_memory_forensics-amiet_pelissier.pdf). **SSTIC 2022**.
* Sylvain Pelissier and Nils Amiet, [*GPG Memory Forensics*](https://nullcon.net/media/media/GPG-memory-forensics-Berlin22.pdf). **Nullcon Berlin 2022**.
* Sylvain Pelissier and Boi Sletterink, [*Practical bruteforce of AES-1024 military grade encryption*](https://pretalx.c3voc.de/media/rc3-2021-r3s/submissions/QMYGR3/resources/Practical_bruteforce_of_AES-1024_military_grad_eMKqvfW.pdf). **rC3 NOWHERE 2021** and **Insomni'hack 2022**
* Sylvain Pelissier, *Unveiling the inner secrets of Electron applications*. **Black Alps 2021**
* Sylvain Pelissier, [*In radare2 /c2 means Cryptography*](https://raw.githubusercontent.com/radareorg/r2con2020/f256f0cb63787abfb73d3d20c1921a8a657429dd/day3/Inr2_cMeansCryptography.pdf) **r2con 2020**
* Nicolas Oberli, Karim Sudki and and Sylvain Pelissier, [*ESIL Side-Channel simulation*](https://github.com/radareorg/r2con2020/raw/f256f0cb63787abfb73d3d20c1921a8a657429dd/day3/ESILside-channelSimulation.pdf). **r2con 2020**
* Sylvain Pelissier and Nicolas Oberli, [*Defeating TLS client authentication using fault attacks*](https://www.balda.ch/publications/hardweario20.pdf). **Hardwear.io USA 2020**
* Sylvain Pelissier, [*When file encryption helps password cracking*](https://cybermashup.files.wordpress.com/2015/05/phdaysv.pdf). **PHDaysV 2015**

# Conference papers
* Yolan Romailler and Sylvain Pelissier, [*Practical Fault Attack against the Ed25519 and EdDSA Signature Schemes*](https://www.romailler.ch/ddl/10.1109_FDTC.2017.12_eddsa.pdf). **FDTC 2017**
* Roman Korkikian, Sylvain Pelissier and David Naccache, *[Blind Fault Attack against SPN Ciphers](resources/publications/blind-fault-attack-against-spn.pdf)*. **FDTC 2014**
* Sylvain Pelissier, Prabhakar T.V., Jamadagni H.S., Venkatesha Prasad, R. and  Ignas Niemegeers, [*Providing security in energy harvesting sensor networks*](/resources/publications/providing-security-in-energy-harvesting-sensor-networks.pdf). **CCNC 2011**
* Sylvain Pelissier, [*Cryptanalysis of Reduced Word Variants of Salsa*](/resources/publications/cryptanalysis-of-reduced-word-variants-of-salsa_weworc_09-copy.pdf). **WEWORC 2009**

# Workshop
* Luca Dolfi, Tommaso Gagliardoni, Adina Nedelcu, 
Marco Macchetti and Sylvain Pelissier, [The Workshop on Cryptographic Code Audit and Capture The Flag (WCCA+CTF)](https://research.kudelskisecurity.com/2024/02/19/wccactf-info-page/). **Eurocrypt 2024**
* Nils Amiet and Sylvain Pelissier, [*Security keys workshop*](https://cybermashup.files.wordpress.com/2023/11/yubikeys-workshop-ph0wn-2023.pdf). **Ph0wn 2023**

# Vulnerabilities and bugs found

* [**Lagrange State Committee Contracts #159**](https://github.com/Lagrange-Labs/lsc-contracts/pull/159): Splitting zero attack against BLS proof of possession.
* [**#YWH-PGM232-121**](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/46): Use of weak hash algorithms in the Swiss Post E-voting.
* [**CVE-2022-47931**](https://research.kudelskisecurity.com/2023/03/23/multiple-cves-in-threshold-cryptography-implementations/): Collision in hash function of io.finnet [threshlib](https://github.com/IoFinnet/threshlib).
* [**Golang #56909**](https://github.com/golang/go/issues/56909): Integer overflow in time/Date.
* [**GnuPG T5977**](https://dev.gnupg.org/T5977): Smartcard PIN not cleared from memory.
* [**John the Ripper #5090**](https://github.com/openwall/john/issues/5090) : 1Password Cloud Keychain plugin buffer overflow.
* [**Golang #51747**](https://github.com/golang/go/issues/51747): math/big: infinite loop in Int.ModSqrt for p = 1
* [**GnuPG T5597**](https://dev.gnupg.org/T5977): First 8 bytes of cache item left in clear in memory after decryption.
* [**John the Ripper #5086**](https://github.com/openwall/john/pull/5086): PEM ciphertext buffer overflow.
* [**fastecdsa #75**](https://github.com/AntonKueltz/fastecdsa/pull/75): Elliptic curve point computation error.
* [**CVE-2021-36751**](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-36751): ENC DataVault 7.1.1W uses an inappropriate encryption algorithm.
* [**CVE-2021-36750**](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-36750): ENC DataVault before 7.2 and VaultAPI v67 mishandle key derivation.
* [**CVE-2014-9687**](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-9687): eCryptfs 104 and earlier uses a default salt to encrypt the mount passphrase.