---
title: "About"
description: "My page"
menu:
  main:
    weight: 1
---

I'm an Applied Cryptography at [Zellic](https://zellic.io). My favorite topics are Cryptography, vulnerability research, reverse engineering and embedded software. I currently doing security code audits on Cryptography algorithms and protocols implementations.

I used to perform hardware attacks like fault attacks using electromagnetic glitch or laser fault injections. I enjoyed finding new fault attacks and new way to exploit them to break systems ranging from simple IoT devices to secure smartcards.

I like playing and organizing CTFs.

Opinions published here are my own and not the views of my employer.