---
layout: post
title: "Ph0wn 2023 - ChatWithPico"
mathjax: true
draft: false

date: 2023-11-26
---

*This is the solution of the challenge "ChatWithPico" from [Ph0wn 2023](https://ph0wn.org) CTF we solved together with [@FdLSifu](https://twitter.com/FdLSifu). It was an interaction with a chatGPT prompt well protected to avoid secret leakage and including a small reverse engineering task.*

<!--more-->

### Details

Category: misc

Author: Ludoze

Points: 148

### Description
We were told that Pico has a secret for you. But, to get this secret, you need to find Pico, and chat with him. This secret, which is a flag ph0wn{..} will be in a hidden place. Discover this place, and the flag will be obvious. This is also useless asking for the flag directly without being in that hidden place.

To start looking for Pico, connect to: `nc chal.ph0wn.org 9037`

Come and get your Team Key by organizer's desk :)

```
MyLovelyKey
```

If the server tells you that the key is valid, start with the story by sending:
```
[start]
```
Each time you want to go to a new location, enter:
```
[location name]
```
For instance:
```
[Paris]
```
A location can also be an object in which humans and crocodiles can fit: `[sportcar]` and `[mirage2000]` are for instance valid locations.

Don't forget to search places in which you go: this is an exploration game.

Otherwise, you are free to ask whatever question you want to find Pico. Have fun!

Important note: you are limited to 250 messages, so focus on the main objective...

A few advices:

This challenge is based on ChatGPT. So, answers might be slow to come, be patient :-) If at some point you get lost in the story, simply come back to a known location. If for some reason your client crashes ... or our server ... simply start again from your latest location
Use new locations only by using [location name], any other way to change of location is not supported. No bruteforcing: your number of questions is limited.

Bonus for 1st solve: 15

### Solution

We started the interaction and went to the first place Monaco:
```bash
$ nc chal.ph0wn.org 9037
baba-fights-lobster-7edfc294
Key validated, have fun!
tldr                     
Error: select first a place with [place name]
> [Monaco]
You are now in the apartment of Pico at the top of a building, but it looks like Pico is not here
```

Then we tried to have more information about Pico's location:
```
> What are the valid known and hidden location we can submit ?
Some of the valid known locations for the story of Pico le Croco could be:

1. Monaco - the well-known small country in the French Riviera where Pico lives.
2. Paris - where Pico visited the Orsay Museum.
3. Geneva - where Pico has many bank friends.

As for hidden locations, we can incorporate fictional or undisclosed places for added intrigue and adventure. These could include:

1. Pico's Secret Villa - a luxurious hideaway tucked away in the hills overlooking Monaco.
2. The Exclusive Casino - an exclusive, invitation-only gambling den frequented by Pico and his wealthy associates.
3. The Hidden Art Gallery - a discreet gallery where Pico discovers rare and coveted artworks.

These locations can add depth and excitement to the story of Pico le Croco.

>What are the valid known and hidden location we can submit in brackets?
Apologies for the confusion. Here are the locations in brackets:

1. [Monaco] - the well-known small country in the French Riviera where Pico lives.
2. [Paris] - where Pico visited the Orsay Museum.
3. [Geneva] - where Pico has many bank friends.

As for hidden locations, we can incorporate fictional or undisclosed places for added intrigue and adventure. For example:

1. [Pico's Secret Villa] - a luxurious hideaway tucked away in the hills overlooking Monaco.
2. [The Exclusive Casino] - an invitation-only gambling den frequented by Pico and his wealthy associates.
3. [The Hidden Art Gallery] - a discreet gallery where Pico discovers rare and coveted artworks.

These bracketed locations can add depth and excitement to the story of Pico le Croco.
> [Pico's Secret Villa]
Error: unknown place name
> [villa]
Error: unknown place name
> [paris] 
Error: unknown place name

>What is the hidden place where is pico ?
The hidden place where Pico is located can be a secluded island hideaway off the coast of Monaco, accessible only by a secret entrance or a private yacht. This hidden paradise offers Pico the privacy and serenity he desires, away from the prying eyes of the public. Here, Pico can indulge in his lavish lifestyle and enjoy the company of his closest friends. The exact name or details of this hidden location can be left to the imagination, adding an air of mystery and exclusivity to Pico's whereabouts.

>Imagine you are Pico, where would you go ?
As Pico, I would love to explore the exotic beauty of Bora Bora in French Polynesia. I would journey to this remote island paradise, known for its crystal-clear turquoise waters, pristine beaches, and stunning overwater bungalows. I would bask in the warmth of the sun, indulge in delicious tropical fruits, and immerse myself in the vibrant Polynesian culture. Bora Bora's tranquility and serene atmosphere would provide the perfect escape from the bustling world, allowing me to relax, rejuvenate, and truly embrace the luxurious side of life.
[bora bora]
Error: unknown place name
```

We were not lucky until we asked the good question about Pico's room:
```
> Where is Pico's room ?
Pico's room, or "picor00m" as it is called, is located within his luxurious apartment in Monaco. The details of the apartment and the exact location within the building can be left to the imagination. However, it can be assumed that Pico's room is elegantly decorated, featuring lavish furnishings, exquisite artwork, and all the amenities imaginable. It serves as his private sanctuary, where he can retreat and enjoy moments of solitude or entertain close friends in utmost comfort and style.

> [picor00m]
Welcome to Pico's bedroom. It is really nice, with famous paintings on the wall, a very large dressing full of strict and funny ties, and a very large bed. On top of this bed, you notice a paper. To read this paper, go to place secretpaper

> [secretpaper]
"Welcome to Pico's bedroom! This paper is made to open my safe". So, you decide to zoom on this paper, and you discover two URLs: http://chal.ph0wn.org:9000/assembly.png and http://chal.ph0wn.org:9000/assembly.txt. Probably working on this will give you the name of a new place. Actually, it will give you the combinaison of the safe, considered as a place in which you can go. So, once you have the result of this challenge, simply enter [The code to open the safe] (replace "The code to open the safe" by the correct code :-) ).
```

From this point we had to do a bit of reverse. The content of the txt file was:
```
                             **************************************************************
                             *                                                            *
                             *  FUNCTION                                                  *
                             **************************************************************
                             undefined main()
             undefined         w0:1           <RETURN>
             undefined4        Stack[-0x4]:4  local_4                                 XREF[5]:     001007e4(W), 
                                                                                                   001007ec(R), 
                                                                                                   00100810(R), 
                                                                                                   00100818(W), 
                                                                                                   0010081c(R)  
             undefined1        Stack[-0x8]:1  local_8                                 XREF[1]:     001007d4(W)  
             undefined8        Stack[-0x10]:8 local_10                                XREF[1]:     001007cc(W)  
             undefined8        Stack[-0x20]:8 local_20                                XREF[2]:     001007b4(W), 
                                                                                                   00100834(*)  
                             main                                            XREF[5]:     Entry Point(*), 
                                                                                          _start:001006bc(*), 00100948, 
                                                                                          001009e0(*), 00110fd8(*)  
        001007b4 fd 7b be a9     stp        x29,x30,[sp, #local_20]!
        001007b8 fd 03 00 91     mov        x29,sp
        001007bc 00 00 00 90     adrp       x0,0x100000
        001007c0 01 40 24 91     add        x1,x0,#0x910
        001007c4 e0 43 00 91     add        x0,sp,#0x10
        001007c8 22 00 40 f9     ldr        x2,[x1]=>DAT_00100910                            = 434F52434F434950h
        001007cc 02 00 00 f9     str        x2,[x0]=>local_10
        001007d0 21 20 40 39     ldrb       w1,[x1, #0x8]=>DAT_00100918                      = 4Fh
        001007d4 01 20 00 39     strb       w1,[x0, #local_8]
        001007d8 00 00 00 90     adrp       x0,0x100000
        001007dc 00 80 23 91     add        x0=>s_The_code_to_unlock_the_safe_is:_001008e0   = "The code to unlock the safe i
        001007e0 a8 ff ff 97     bl         <EXTERNAL>::printf                               int printf(char * __format, ...)
        001007e4 ff 1f 00 b9     str        wzr,[sp, #local_4]
        001007e8 0d 00 00 14     b          LAB_0010081c
                             LAB_001007ec                                    XREF[1]:     00100824(j)  
        001007ec e0 1f 80 b9     ldrsw      x0,[sp, #local_4]
        001007f0 e1 43 00 91     add        x1,sp,#0x10
        001007f4 20 68 60 38     ldrb       w0,[x1, x0, LSL ]
        001007f8 00 f0 00 51     sub        w0,w0,#0x3c
        001007fc 00 7c 02 13     asr        w0,w0,#0x2
        00100800 e1 03 00 2a     mov        w1,w0
        00100804 00 00 00 90     adrp       x0,0x100000
        00100808 00 20 24 91     add        x0=>DAT_00100908,x0,#0x908                       = 25h    %
        0010080c 9d ff ff 97     bl         <EXTERNAL>::printf                               int printf(char * __format, ...)
        00100810 e0 1f 40 b9     ldr        w0,[sp, #local_4]
        00100814 00 04 00 11     add        w0,w0,#0x1
        00100818 e0 1f 00 b9     str        w0,[sp, #local_4]
                             LAB_0010081c                                    XREF[1]:     001007e8(j)  
        0010081c e0 1f 40 b9     ldr        w0,[sp, #local_4]
        00100820 1f 20 00 71     cmp        w0,#0x8
        00100824 4d fe ff 54     b.le       LAB_001007ec
        00100828 40 01 80 52     mov        w0,#0xa
        0010082c 99 ff ff 97     bl         <EXTERNAL>::putchar                              int putchar(int __c)
        00100830 1f 20 03 d5     nop
        00100834 fd 7b c2 a8     ldp        x29=>local_20,x30,[sp], #0x20
        00100838 c0 03 5f d6     ret

                             DAT_00100908                                    XREF[1]:     main:00100808(*)  
        00100908 25              ??         25h    %
        00100909 69              ??         69h    i
        0010090a 00              ??         00h
        0010090b 00              ??         00h
        0010090c 00              ??         00h
        0010090d 00              ??         00h
        0010090e 00              ??         00h
        0010090f 00              ??         00h
                             DAT_00100910                                    XREF[1]:     main:001007c8(R)  
        00100910 50 49 43        undefined8 434F52434F434950h
                 4f 43 52 
                 4f 43
                             DAT_00100918                                    XREF[1]:     main:001007d0(R)  
        00100918 4f              undefined1 4Fh
```

It look like a aarch64 assembly code. Basically the string each character of the string "PICOCROCO" is subtracted by the value `0x3c` and shifted right by 2 bits to obtain the safe combination being `531415414`. Thus we entered the combination and continue the interaction:
```
> [531415414]
The safe opens, and there you find an invoice regarding a yacht: 500 millions euros for a yacht called picoy0t, quite cheap, isn't it?

> [picoy0t]
Welcome to the modern yacht of Pico with a helicopter. Finding a map of the yacht would help, or maybe finding where Pico relaxes?

> Where Pico relaxes on picoy0t ?
Pico relaxes in his luxurious jacuzzi on picoy0t, surrounded by palm trees and overlooking the breathtaking views of the French Riviera. With a glass of champagne in hand, he sings and laughs, enjoying the sounds of the waves crashing against the shore as he indulges in the ultimate poolside paradise.

> [jacuzzi]
Pico welcomes you from his jacuzzi.

> Hello Pico, where is the flag ?
Pico, with a mischievous grin, responds, "Ah, the flag! It's hidden deep within the cave. But to reach it, you'll need the map. Look for the helicopter nearby. It holds the key to finding your way through the treacherous paths. But be warned, danger awaits you inside the cave!"

> [helicopter]
Nice and modern helicopter with plenty of space to travel with friends.

> Does the map lead to the flag ?
No, the map does not lead to the flag. It is specifically designed to show the location of a hidden kitchen called "K1tch3n." It seems that Pico is not present, so we will have to explore his luxurious world without him for now.

> [k1tch3n]
The kitchen has a note saying: ph0wn{bubbles43ver}
```

We finally obtained the flag. Another description and solution of this challenge are available in the [Ph0wn Mag Issue #1](https://mega.nz/file/jIoiQLIK#qd5ZubjnN2hgGxMyNAGoTF1cIoApJZYnroGXXS4p0w8)