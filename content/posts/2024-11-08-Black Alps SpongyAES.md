---
layout: post
title: "Black Alps 2024 - SpongyAES
"
mathjax: true
draft: false

date: 2024-11-09
---

*This is the solution of the "SpongyAES" challenge of [Black Alps 2024](https://www.blackalps.ch/ba-24) solved together with [@FdLSifu](https://twitter.com/FdLSifu). The challenge was a hash function using a sponge construction with a too small capacity.*

<!--more-->

### Details

Category: Crypto

Author: Alexandre Duc

Points: 460

### Description

The flag was hashed using a custom sponge-based hash function.

You know that the flag length is 19 ASCII characters (including BA24{...})

The rate is 15

Download Links: [sponge.py](/resources/2024/blackalps/spongyaes/sponge.py) [parameteres.txt](/resources/2024/blackalps/spongyaes/parameters.txt)

### Solution

After reading the script we noticed that the hash function is a sponge construction with the output being 64 bytes:

```python
>>> from base64 import b64decode
>>> b64decode(hash)
>>> len(b64decode(hash))
64
```

Recall that the sponge construction is done like that:

![Sponge construction](/resources/2024/blackalps/spongyaes/SpongeConstruction.png)

But in this case, the rate is $r=15$ and the capacity is $c=1$. The permutation layer $f$ is the AES encryption using the key `07070707070707070707070707070707`. The input is padded to the next multiple of 15 bytes. So, we have two blocks during the absorbing phase, $P_0$ and $P_1$ and $\lceil \frac{64}{15} \rceil = 5$ output blocks $Z_0, \dots, Z_4$ during the squeezing phase. The first thing we notice is that we can compute the full state of the sponge construction before $Z_1$ by bruteforcing the byte of the capacity:

```python
#unsqueezing
state = hash[0:15]
for i in range(256):
    s = cipher.encrypt(state + bytes([i]))
    if hash[15:30] == s[0:15]:
        print(f"Found capacity: {i}")
        break
```

We can inverse the permutation to obtain the full state before $Z_0$ but after beeing xored with $P_1$:

```python
state = cipher.decrypt(hash[0:15] + bytes([i]))
```

We also know that the last input state $P_1$ has 4 bytes of the flag with the last byte is `b"}"` and then we have 11 bytes of padding. So we can bruteforce the three unknown bytes until the state inverts properly to a state starting with `b"BA24{`:


```python
# unabsorbing
for i in range(256):
    for j in range(256):
        for k in range(256):
            s = strxor(bytes([i]) + bytes([j]) + bytes([k]) + b"}" + b"\x80" + b"\x00"*11, state)
            s = cipher.decrypt(s)
    
            if s.startswith(b"BA24{") :
                print(s[0:15] + bytes([i]) + bytes([j]) + bytes([k]) + b"}")
```

This outputs the flag:
```bash
Found capacity 144
b'BA24{C4p4c1tyisk3y}'
```