---
layout: post
title: "Root-Xmas 2024 Day 14 - Almost a Gift
"
mathjax: true
draft: true

date: 2024-12-15
---

*This is the solution of the "Day 14 - Almost a Gift" challenge of [Root-Xmas](https://xmas.root-me.org) 2024. The challenge .*

<!--more-->

### Details

Points: 1

Category: Crypto

Author : Cryptanalyse

### Description

Root-Me's Santa has been very generous in the past few days, but now come the time to be generous and slightly challenging at the same time. Unwrap those gifts to recover the flag Santa has prepared for you.

### Solutions

The script is simple and we search a flag which is encrypted with RSA public key $n = R e$ where $R$ and $e$ are 1337 bits each. We also have "gift" values:

R \cdot O + v_0
R \cdot o + v_1
R \cdot t + v_2
R \cdot M + v_3

Where each $v_i$ is randomly generated of 666 bits.