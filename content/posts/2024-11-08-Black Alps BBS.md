---
layout: post
title: "Black Alps 2024 - BBS
"
mathjax: true
draft: false

date: 2024-11-08
---

*This is the solution of the "BBS" challenge of [Black Alps 2024](https://www.blackalps.ch/ba-24) solved together with [@cryptopathe](https://crypto.junod.info). The challenge was a BBS PRNG with additional values allowing to reverse the algorithm.*

<!--more-->

### Details

Category: Crypto

Author: Alexandre Duc

Points: 500

### Description

A weird (?) PRNG is used to mask the flag.

You are magically given some additional useful information.

Download Links: [bbs.sage](/resources/2024/blackalps/bbs/bbs.sage) [parameteres.txt](/resources/2024/blackalps/bbs/parameters.txt)

### Solution

The sage script uses the [Blum Blum Shub](https://en.wikipedia.org/wiki/Blum_Blum_Shub) (BBS) pseudorandom number generator to encrypt the flag. It uses a seed $s$ which is squared at each step modulo $n = pq$. The parity bit of the result is outputed as a random bit. It seems there is no flaws in the BBS implementation itself. We do not have access to the current seed value $s$. However we were given two additional values as the output of the PRNG: $r_q = s + q$ and $r_p = s +q$. We figure out that if we substract those tow values we got $q-p$. Then after searching a bit and asking ChatGPT we figure out that by setting $d = q-p$ we have the second order equation:

$$n = (q+d)q = q^2 + dq$$

Solving this equation gives use the value of $q$ and then the value of $p$:

```bash
sage: attach("parameters.py")
sage: d = rq-rp
sage: eq = x^2 + d*x - n
sage: eq.solve("x")
[x == 27295610749246259153941624094717491595222535878157189705308316982981029586502155418784292314320246063057810616840036684347813602259993535019783490350448192568419362459297578771625304135238380731608053506300827309084536407466927127703588671925699979734062333402144219858619565249267242025516199402954974161223, x == -105464683303416371040899806069948780079301435394351207529815137912507055093642417720712835098619946705449238098926229346607383066238338094360441059768687094495224809773352198049160800828171020094925104043637599005780685501082253181019411515410481588890065401181858224499016448477217557078952005170822345426111]
```

The first value give the correct value for $q$.

Now that we have $q$ and $p$ we can reverse the BBS algorithm to recover the initial seed and decrypt the flag. It means that we compute the square root of the previous seed modulo $p$ and modulo $q$, we reconstruct the four possible values for the square root with the CRT. As explained by **Theorem 9** in the nice [paper](https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=b816731d0906eef20cc9cbb6f4cc299cc27f4dfe) of cryptopathe only one of them will be a quadratic residue mouldo $n$. So we choose this one as the new seed and we loop. 

Here is the complete script to decrypt the flag:
```python
from Crypto.Util import strxor

n = 2878722943242584369487577709674021473361830051453027816222804069858926659637346489807660154713889512285202934429001936267583473898531401325035139081590347904842058221685484378041564611477585984431622907947108588828302518036113732770729622949515878791111211973359801381541663020813018610329103958348485213471145209293006150761531986811559187488891892251149531888050169292545509771545640667324151828599863456305492879256014602427873006312859290462892295761185955854271796551785280865032525949062613335214776115714307573221830081083866338378146259997997158901638391839800506491658631375803111197850961542067922747893753
p = 27295610749246259153941624094717491595222535878157189705308316982981029586502155418784292314320246063057810616840036684347813602259993535019783490350448192568419362459297578771625304135238380731608053506300827309084536407466927127703588671925699979734062333402144219858619565249267242025516199402954974161223

q = n//p

seed = 1880973399310866294973939337139366236280606084465082017500958859540154610474303394665773548131061331306856359141133545841512502267010055611606065595538461387486410529787373541155497233953832913396509173565177246874399830311110462707707083418823381208538109019326703842038778528140208889070332780513599527745486315676130187874332433280573756573179422503773498828875141205305902868489331102748082962909263613803450433352252543135449970735098174673903068765109296203802489776173893555099583715055309349110314075294464794389316694398437432918683167576983724260984194113810261101217532609312975804558691087394149987864457

print(p*q == n)

number_bytes = 31

cb = b'\xf1\xd5kI\x06\xa6?\x18\xe6\xb2IK\xd4\xcc\xe8\x88k\xdf\x1ccs\x16\x1577\xb4\xf8\x0e\xa7>\x10'

Fp = GF(p)
Fq = GF(q)

def square_rec(new_seed):
    for i in range(len(cb)*8):
        s1 = Fp(new_seed).sqrt()
        #print(new_seed)
        s2 = Fq(new_seed).sqrt()
        t = []
        t.append(crt([int(s1), int(s2)], [p, q]))
        t.append(crt([int(s1), int(q-s2)], [p, q]))
        t.append(crt([int(p-s1), int(s2)], [p, q]))
        t.append(crt([int(p-s1), int(q-s2)], [p, q]))
        
        for s in t:
            #print(s^2 % n == new_seed)
            if jacobi_symbol(Fp(s), p) == 1 and jacobi_symbol(Fq(s),q) == 1:
                new_seed = s
                break
    return new_seed

def bbs(seed, number_bytes, n):
    ret = 0
    for _ in range(number_bytes*8):
        seed = pow(seed, 2, n)
        ret <<= 1
        ret |= (seed % 2)
    return ret.to_bytes(number_bytes), seed

seed = square_rec(seed)
random, seed = bbs(seed, 31, n)

print(strxor.strxor(cb, random))
```
