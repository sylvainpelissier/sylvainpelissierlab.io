---
layout: post
title: "NorthSec CTF 2023 - HR Complaints"
mathjax: true
draft: false

date: 2023-05-23
---

*This is the solution of the challenge "HR Complaints" from [NorthSec 2023](https://nsec.io/competition/). It was an exploitation of the ROCA vulnerability on a 512-bit long RSA key.*

<!--more-->

### Description

*Registry of Confidential Artifacts*

*If you see something, snitch someone*

*Management at the corporation is not aware of everything happening at work. Management is not fully aware of your deepest thoughts and anxieties. Despite our tremendous network of cameras, we still dont have 100% monitoring rate on every employees.*

*As such, Management implemented a new KPI. Employees are expected to tattletale on employees at least twice a month.*

*Please use our convenient, anonymous and encrypted service.*

*Here are the following step:*
* *Create a textfile (data.txt) and write your complaint.*
* *Download the public key here . Name the key [key.pub](resources/2023/northsec/hrcomplaints/key.pub)*
* *Download and run the script [here](resources/2023/northsec/hrcomplaints/encrypt.sh)*
* *Upload the encrypted files*

*Upload new File*

### Details

Category: Cryptography

Points: 3

Author: Laurent Desaulniers

### Solution

After uploading a file we could notice that the file is listed in `/upload`. 6 other files were presents:
* [8746323478246B0B0.bin.enc](resources/2023/northsec/hrcomplaints/8746323478246B0B0.bin.enc)
* [8746323478246B0B0.enc](resources/2023/northsec/hrcomplaints/8746323478246B0B0.enc)
* [8945783518973D00D.bin.enc](resources/2023/northsec/hrcomplaints/8945783518973D00D.bin.enc)
* [8945783518973D00D.enc](resources/2023/northsec/hrcomplaints/8945783518973D00D.enc)
* [8976785674735N00B.bin.enc](resources/2023/northsec/hrcomplaints/8976785674735N00B.bin.enc)
* [8976785674735N00B.enc](resources/2023/northsec/hrcomplaints/8976785674735N00B.enc)

After trying different guess, it appear that the public key is vulnerable to the [ROCA vulnerability](https://en.wikipedia.org/wiki/ROCA_vulnerability). The title was also an indication. There is a tool to test if the key is vulnerable called `roca-detect`:

```bash
$ pip install roca-detect -U
$ roca-detect key.pub
2023-05-25 14:14:03 [537677] WARNING Fingerprint found in PEM RSA key key.pub 
{"type": "pem-rsa-key", "fname": "key.pub", "idx": 0, "pem": "-----BEGIN PUBLIC KEY-----\nMFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKYwxOfFzvKmESBBbgROmbRFY7CU95Ff\nxUgtZJ1BUmi1MA74v234neyPyd8YEoL+kEBwuKQk4kMcejgLRaizetsCAwEAAQ==\n-----END PUBLIC KEY-----", "e": "0x10001", "n": "0xa630c4e7c5cef2a61120416e044e99b44563b094f7915fc5482d649d415268b5300ef8bf6df89dec8fc9df181282fe904070b8a424e2431c7a380b45a8b37adb", "marked": true, "time_years": 0.00013349807611760526, "price_aws_c4": 0.05851220676234639}
2023-05-25 14:14:03 [537677] INFO ### SUMMARY ####################
2023-05-25 14:14:03 [537677] INFO Records tested: 1
2023-05-25 14:14:03 [537677] INFO .. PEM certs: . . . 0
2023-05-25 14:14:03 [537677] INFO .. DER certs: . . . 0
2023-05-25 14:14:03 [537677] INFO .. RSA key files: . 1
2023-05-25 14:14:03 [537677] INFO .. PGP master keys: 0
2023-05-25 14:14:03 [537677] INFO .. PGP total keys:  0
2023-05-25 14:14:03 [537677] INFO .. SSH keys:  . . . 0
2023-05-25 14:14:03 [537677] INFO .. APK keys:  . . . 0
2023-05-25 14:14:03 [537677] INFO .. JSON keys: . . . 0
2023-05-25 14:14:03 [537677] INFO .. LDIFF certs: . . 0
2023-05-25 14:14:03 [537677] INFO .. JKS certs: . . . 0
2023-05-25 14:14:03 [537677] INFO .. PKCS7: . . . . . 0
2023-05-25 14:14:03 [537677] INFO Fingerprinted keys found: 1
2023-05-25 14:14:03 [537677] INFO WARNING: Potential vulnerability
2023-05-25 14:14:03 [537677] INFO ################################
```

It appeared that the key is indeed vulnerable to the ROCA vulnerability. [RsaCtfTool](https://github.com/RsaCtfTool/RsaCtfTool) implements the key recovery for such keys:

```bash
~/software/RsaCtfTool/RsaCtfTool.py --private --attack roca --publickey key.pub --timeout 7200
[*] Testing key key.pub.
[*] Performing roca attack on key.pub.
[*] Attack success with roca method !

Results for key.pub:

Private key :
-----BEGIN RSA PRIVATE KEY-----
MIIBOwIBAAJBAKYwxOfFzvKmESBBbgROmbRFY7CU95FfxUgtZJ1BUmi1MA74v234
neyPyd8YEoL+kEBwuKQk4kMcejgLRaizetsCAwEAAQJAeWhlYMCkR6XEGyFRgvn8
1bJfeo/JMD2bvPfUagFfd7ey8CkazpR8VVPecHAAu6tnOQrDKZZayAIAash59Zg7
yQIhARZKENDPWt9VYUuRKgoyjAlvrLKuBK+XraBIBHysZXy3AiEAmOEvrGdSKMXh
rHwwo7ugwfZGdo6zXiT7pE7Ylo1Mlv0CIQCPpMNxRNa/GK8T9mYgxV6LMF2QDUrV
6BDy/rlyMPphmQIgOQj6AjXhvvhkQzdp5gvf47z/eMewP1VI1Hnd7/zzL/kCIQDP
ybGNqNEhCqLOM1kTep+WBEMxn5yrWB1bxxH2XH+OyA==
-----END RSA PRIVATE KEY-----
```
After about 42 minutes we got the corresponding private key and we saves it in the file `ras.priv`. The trick here is to have a large enough timeout value or `RsaCtfTool` will stop before finding the key.

Then the encryption script can be adapted to decrypt all the files from the private key:
```bash
USERNAME=8976785674735N00B 
openssl rsautl -decrypt -inkey rsa.priv -in $USERNAME.bin.enc -out $USERNAME.bin
openssl enc -d -aes-256-cbc -in $USERNAME.enc -out $USERNAME.txt -pass file:./$USERNAME.bin
cat $USERNAME.txt
```

And the last file gave us the flag:
```
$ ./decrypt.sh
Complaint #3249
I dont understand why #8976785674735B077 does not like my Hawaian shirt day idea. This and more pieces of flair would improve the office spirit.
FLAG-3a7838a533989e2891851e8a0c0ee2fd65f3cc96
```
